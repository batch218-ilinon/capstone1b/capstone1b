import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

import edsonLogo from '../images/edsonLogo.png';

export default function Header(){
	return(
		<>
		<Container fluid className="headerWrapper p-md-4 d-none d-md-block">
			<Row className="justify-content-center">
				<Col lg={11} xl={10}>
					<Row className="justify-content-between align-content-center">
						<Col md={3} lg={2} xl={1}>
							<a href="/">
							<img className="portfolioLogo" src={edsonLogo} alt="logo" />
							</a>
						</Col>
						<Col md={6} lg={4} xl={4} className="align-content-center">
							<Row className="align-content-center">
								<Col className="text-center">
									<a href="#projectSection">Projects</a>
								</Col>
								<Col className="text-center">
									<a href="#skillsSection">Skills</a>
								</Col>
								<Col className="text-center">
									<a href="#contactSection">Contact</a>
								</Col>
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
		</Container >
		<Navbar bg="light" expand="lg" className="d-md-none sticky-top">
		  <Container>
		    <Navbar.Brand href="/"><img className="portfolioLogo" src={edsonLogo} alt="logo" /></Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link href="#projectSection" className="headerLinks">Projects</Nav.Link>
		        <Nav.Link href="#skillsSection" className="headerLinks">Skills</Nav.Link>
		        <Nav.Link href="#contactSection" className="headerLinks">Contact</Nav.Link>
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		</>
	)
}