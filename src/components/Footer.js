import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

export default function Footer(){
	return(
		<Container className="footerWrapper py-4">
			<Row>
				<Col className="text-center">
					<div className="footerLabel">
						&#169; Jun Edson Ilinon | All Rights Reserved 2023
					</div>
				</Col>
			</Row>
		</Container>
	)
}