import Header from './components/Header'
import Hero from './section/Hero'
import Project from './section/Project'
import Skills from './section/Skills'
import Contact from './section/Contact'
import Footer from './components/Footer'

import Container from 'react-bootstrap/Container';

export default function App() {
  return (
    <div id="portfolioContents">
      <Header />
      <Hero />
      <Project />
      <Skills />
      <Contact />
      <Footer />
    </div>
  );
}
