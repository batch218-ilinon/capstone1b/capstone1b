import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";

import { Navigation, Autoplay } from "swiper";

import homePageFigma from '../images/homePageFigma.JPG';
import productPageFigma from '../images/productPageFigma.JPG';
import loginPageFigma from '../images/loginPageFigma.JPG';
import registerPageFigma from '../images/registerPageFigma.JPG';

import EcomWeb from '../images/EcomWeb.png';
import EcomProductPage from '../images/EcomProductPage.png';
import EcomLoginPage from '../images/EcomLoginPage.png';
import EcomRegisterPage from '../images/EcomRegisterPage.png';


export default function Project(){
	return(
		<Container fluid className="projectWrapper" id="projectSection">
			<Row>
				<Col className="text-center">
					<div className="projectHeading">
						Projects
					</div>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md={10} xl={8}>
					<Row>
						<Col lg={6} xl={6}>
							<div className="projectTitle">
								TopShop
							</div>
							<div className="projectType mb-5">
								E-commerce Website
							</div>
							<div className="technologyTitle mb-2">
								Technologies
							</div>
							<div className="projectParagraph">
								<b className="technologySubTitle">Web design:</b> Figma
							</div>
							<div className="projectParagraph">
								<b className="technologySubTitle">Frontend:</b> React, HTML, CSS, Bootstrap
							</div>
							<div className="projectParagraph">
								<b className="technologySubTitle">Backend:</b> Express.js, Mongoose, MongoDB
							</div>
							<Row className="designSlide d-none d-lg-block">
								<Col>
									<Swiper 
										navigation={true} 
										className="mySwiper"
										autoplay={{
										  delay: 3000,
										  disableOnInteraction: false,
										}}
										modules={[Autoplay, Navigation]}
									>
									  <SwiperSlide className="text-center"><img src={homePageFigma} alt="Web design" class="img-fluid"/></SwiperSlide>
									  <SwiperSlide className="text-center"><img src={productPageFigma} alt="Web design" class="img-fluid"/></SwiperSlide>
									  <SwiperSlide className="text-center"><img src={loginPageFigma} alt="Web design" class="img-fluid"/></SwiperSlide>
									  <SwiperSlide className="text-center"><img src={registerPageFigma} alt="Web design" class="img-fluid"/></SwiperSlide>
									</Swiper>
								</Col>
							</Row>
						</Col>
						<Col lg={6} xl={6} className="pt-5 pt-lg-0">
							<Row>
								<Swiper 
									navigation={true} 
									className="mySwiper d-none d-md-block"
									autoplay={{
									  delay: 3000,
									  disableOnInteraction: false,
									}}
									modules={[Autoplay, Navigation]}
								>
								  <SwiperSlide className="text-center mt-5"><img src={EcomWeb} alt="Web design" class="img-fluid"/></SwiperSlide>
								  <SwiperSlide className="text-center"><img src={EcomProductPage} alt="Web design" class="img-fluid"/></SwiperSlide>
								  <SwiperSlide className="text-center mt-5"><img src={EcomLoginPage} alt="Web design" class="img-fluid"/></SwiperSlide>
								  <SwiperSlide className="text-center mt-5"><img src={EcomRegisterPage} alt="Web design" class="img-fluid"/></SwiperSlide>
								</Swiper>
								<Swiper 
									navigation={true} 
									className="mySwiper d-md-none"
									autoplay={{
									  delay: 3000,
									  disableOnInteraction: false,
									}}
									modules={[Autoplay, Navigation]}
								>
								  <SwiperSlide className="text-center mt-5"><img src={EcomWeb} alt="Web design" class="img-fluid"/></SwiperSlide>
								  <SwiperSlide className="text-center"><img src={EcomProductPage} alt="Web design" class="img-fluid"/></SwiperSlide>
								  <SwiperSlide className="text-center">
									  <img src={EcomLoginPage} alt="Web design" className="img-fluid my-3"/> 
									  <img src={EcomRegisterPage} alt="Web design" className="img-fluid my-3"/>
								  </SwiperSlide>

								</Swiper>
							</Row>
							<Row className="text-center">
								<div className="visitWeb">Visit Website</div>
							</Row>
							<Row className="text-center">
								<a href="https://top-shop-five.vercel.app" target="_blank">
								<div className="webLink">https://top-shop-five.vercel.app/</div>
								</a>
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>
	)
}