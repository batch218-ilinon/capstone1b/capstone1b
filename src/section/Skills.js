import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import htmlLogo from '../images/htmlLogo.png';
import cssLogo from '../images/cssLogo.png';
import javascriptLogo from '../images/javascriptLogo.png';
import bootstrapLogo from '../images/bootstrapLogo.png';

import mongoDBLogo from '../images/mongoDBLogo.png';
import expressLogo from '../images/expressLogo.png';
import reactLogo from '../images/reactLogo.png';
import nodeJsLogo from '../images/nodeJsLogo.png';

import postmanLogo from '../images/postmanLogo.png';
import sublimeLogo from '../images/sublimeLogo.png';
import herokuLogo from '../images/herokuLogo.png';
import gitLogo from '../images/gitLogo.png';

export default function Skills(){
	return(
		<Container fluid className="skillsWrapper" id="skillsSection">
			<Row>
				<Col>
					<div className="skillsHeading text-center">
						Skills
					</div>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col lg={10} xl={8}>
					<Row className="justify-content-between">
						<Col lg={3} className="mb-5 mb-lg-0">
							<Row className="justify-content-center">
								<Col xs={12}>
									<div className="skillsTitle text-center">
										Front End
									</div>
								</Col>
								<Col md={6} lg={12}>
									<Row>
										<Col xs={6} className="text-center">
											<img src={htmlLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												HTML
											</div>
										</Col>
										<Col xs={6} className="text-center">
											<img src={cssLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												CSS
											</div>
										</Col>
									</Row>
									<Row>
										<Col xs={6} className="text-center">
											<img src={javascriptLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Javascript
											</div>
										</Col>
										<Col xs={6} className="text-center">
											<img src={bootstrapLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Bootstrap
											</div>
										</Col>
									</Row>
								</Col>
								<Col xs={12}>
								</Col>
							</Row>
						</Col>
						<Col lg={3} className="mb-5 mb-lg-0">
							<Row className="justify-content-center pt-5 pt-lg-0">
								<Col xs={12}>
									<div className="skillsTitle text-center">
										MERN
									</div>
								</Col>
								<Col md={6} lg={12}>
									<Row>
										<Col xs={6} className="text-center">
											<img src={mongoDBLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												MongoDB
											</div>
										</Col>
										<Col xs={6} className="text-center">
											<img src={expressLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Express
											</div>
										</Col>
									</Row>
									<Row>
										<Col xs={6} className="text-center">
											<img src={reactLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												React
											</div>
										</Col>
										<Col xs={6} className="text-center">
											<img src={nodeJsLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Node.js
											</div>
										</Col>
									</Row>
								</Col>
								<Col xs={12}>
								</Col>
							</Row>
						</Col>
						<Col lg={3}>
							<Row className="justify-content-center pt-5 pt-lg-0">
								<Col xs={12}>
									<div className="skillsTitle text-center">
										Other Tools
									</div>
								</Col>
								<Col md={6} lg={12}>
									<Row>
										<Col xs={6} className="text-center">
											<img src={postmanLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Postman
											</div>
										</Col>
										<Col xs={6} className="text-center">
											<img src={sublimeLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Sublime
											</div>
										</Col>
									</Row>
									<Row>
										<Col xs={6} className="text-center">
											<img src={herokuLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Heroku
											</div>
										</Col>
										<Col xs={6} className="text-center">
											<img src={gitLogo} alt="Web design" class="img-fluid"/>
											<div className="skill">
												Git
											</div>
										</Col>
									</Row>
								</Col>
								<Col xs={12}>
								</Col>
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>
	)
}