import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import facebook from '../icons/facebook.svg';
import linkedin from '../icons/linkedin.svg';
import gitlab from '../icons/gitlab.png';
import emailIcon from '../icons/email.svg';

export default function Contact(){
	return(
		<Container fluid className="contactWrapper" id="contactSection">
			<Row>
				<Col>
					<div className="contactHeading text-center">
						Contact
					</div>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md={10} xl={8}>
					<div className="socialMediaHeading">
						Social Media
					</div>
					<Row className="justify-content-between logoWrapper">
						<Col >
							<a href="https://www.facebook.com/edson.salilid.9" target="_blank">
							<img src={facebook} alt="Facebook logo" className="contactIcons"/>
							</a>
						</Col>
						<Col className="text-center">
							<a href="https://www.linkedin.com/in/jun-edson-ilinon/" target="_blank">
							<img src={linkedin} alt="LinkedIn logo" className="contactIcons"/>
							</a>
						</Col>
						<Col className="text-center">
							<a href="https://gitlab.com/je.s.ilinon" target="_blank">
							<img src={gitlab} alt="GitLab logo" className="contactIcons"/>
							</a>
						</Col>
						<Col className="text-end">
							<a href="mailto:je.s.ilinon@gmail.com">
							<img src={emailIcon} alt="Email icon" className="contactIcons"/>
							</a>
						</Col>
					</Row>
					<div className="text-center contactParagraph">
						I am currently available for a job.
					</div>
					<div className="text-center contactParagraph">
						Message me thru my social media accounts or by emailling
					</div>
					<div className="text-center contactParagraph">
						<a href="mailto:je.s.ilinon@gmail.com" id="emailAdd">
						je.s.ilinon@gmail.com
						</a>
					</div>
				</Col>
			</Row>
		</Container>
	)
}
