import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Button from 'react-bootstrap/Button';

import arrowDown from '../icons/arrowDown.svg';

export default function Hero(){
	return(
		<>
		<Container fluid className="heroWrapper">
			<Row>
				<Col lg={{span:10, offset:1}} xl={{span:10, offset:1}}>
					<div className="firstHeroHeader">
						Hi, I am Edson,
					</div>
					<div className="secondHeroHeader mt-2 mt-lg-0">
						a Full Stack Developer
					</div>
				</Col>
			</Row>
		</Container>
		<Container fluid className="mt-5 heroParagraphWrapper">
			<Row>
				<Col lg={{span:10, offset:1}} xl={{span:10, offset:1}}>
					<div className="heroParagraph">
						I enjoy solving problems with computer programming.
					</div>
				</Col>
			</Row>
			<Row className="justify-content-center projectButtonWrapper">
				<Col lg={10} xl={8} className="text-end">
					<a href="#projectSection">
					<Button className="projectButton" variant="primary" type="submit" id="submitBtn">
					See My Work <img className="arrowIcon" src={arrowDown} />
					</Button>
					</a>
				</Col>
			</Row>
		</Container>
		</>
	)
}